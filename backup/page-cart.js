import { Container} from 'react-bootstrap';
import '../cartStyles.css'

export default function Cart(){
 
 	// const [cartData, setCartData] = useState([])
 
 	const [productsData, setProductsData] = useState([])
	let products = []
	useEffect(() => {
 
		fetch('http://localhost:4000/products')
		.then(res => res.json())
		.then(data => {
			setProductsData(data)
		})
	}, [])
 
	products = productsData.map(product => {
		return(

			<CartItem key={product._id} productProp={product}/>
		)
	})

	
 


	return(
		<>
			<Container>
				<h1>Your Cart</h1>
				<ul className="cart-listing__list">
					<li className="cart-listing__header">
						<div className="cart-listing__header-name">
							<span>Name</span>
						</div>
						<div className="cart-listing__header-price">
							<span>Price</span>
						</div>
						<div className="cart-listing__header-quantity">
							<span>Quantity</span>
						</div>
						<div className="cart-listing__header-action">
							<span>Action</span>
						</div>
					</li>
					
					{products}
					
				</ul>
			</Container>
		</>
	)

	
} 