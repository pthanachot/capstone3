import { useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import { Link, Navigate, useHistory, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
 
export default function Login() {
    const {user, setUser} = useContext(UserContext);
    const navigate = useNavigate();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isLoginDisabled, setIsLoginDisabled] = useState(false);

    useEffect(() => {
        if (email === '' || password === '') {
            setIsLoginDisabled(true);
        } else {
            setIsLoginDisabled(false);
        }
        // Alternative Logic
        // setIsLoginDisabled(email !== '' || password !== '');
    }, [email, password]);

    const login = (e) => {
        e.preventDefault()
        fetch("http://localhost:4000/users/login", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data)
            if(data.accessToken) { //check if accessToken was receive, which means user successfully authenticated
                //save the user's token to their localStorage using setItem so that it can be viewd by all our page when it needs to
                                    //the form is like is because its the KEY VALUE PAIR, it will save Key of token and Value of data.accessToken in localstorage
                setUser("");
                localStorage.setItem('token', data.accessToken)
                navigate("/", { replace: true });
                fetch("http://localhost:3000/users/details",{
                    headers: {
                        Authorization: `Bearer ${data.accessToken}`
                    }
                })
                .then(res => res.json())
                .then(data => {
                    // console.log(data)
                    localStorage.setItem("id", data._id)
                    localStorage.setItem("isAdmin", data.isAdmin)
                    Swal.fire({
                            title: "Successfully Log In",
                            icon: 'success',
                            text: "You have been successfully log in"
                        })
                    // window.location.replace("./courses.html")
                })

            } else {
                Swal.fire({
                            title: "Log In Failed",
                            icon: 'error',
                            text: "Authentication failed. Please try again"
                        })
            }
        })
    }
 
    // const login = async (e) => {
    //     e.preventDefault();

    //     const payload = {
    //         method: 'POST',
    //         headers: {
    //             'Content-Type':'application/json'
    //         }, 
    //         body: JSON.stringify({ email, password })
    //     };


    //     const response = await fetch('http://localhost:4000/users/login', payload);
    //     const data = await response.json();

    //     if (typeof data.accessToken !== 'undefined') {
    //         setUser("");
    //         localStorage.setItem('token', data.accessToken)
    //         navigate("/", { replace: true });
          

    //     } else {
    //         Swal.fire('Login Failed', 'Credentials not found, try again', 'error');
    //     }
       
    // }

    // const getUserData = async (token) => {
    //     const payload = {
    //         headers: {
    //             'Authorization':`Bearer ${token}`
    //         }
    //     };
    //     const response = await fetch('http://localhost:4000/users/details', payload);
    //     const data = await response.json();
        
    //     localStorage.setItem('id', data._id);
    //     localStorage.setItem('isAdmin', data.isAdmin);

    // }


    return (
        // (user._id !== null) ?
        // //     // (data.isAdmin === true) ? 
        // //         // <Redirect to="/admin" />
        // //         // : 
        //         <Navigate to="/" />
        //     :
        <Container>
            <h3>Login</h3>
            <Form className="mt-3" onSubmit={login}>
                <Form.Group>
                    <Form.Label>First Name </Form.Label>
                    <Form.Control type="text" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Enter password" value={password} onChange={e => setPassword(e.target.value)} required />
                </Form.Group>
                <Button type="submit" variant="success" disabled={isLoginDisabled}>Login</Button>
            </Form>
        </Container>
    )
}







