import { useState, useEffect } from 'react';
import AdminItem from '../components/AdminItem';
// import { Container} from 'react-bootstrap';
import { Container, Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';


import '../adminDashboardStyles.css'

export default function Admin(){
  
 	const [name, setName] =  useState("")
	const [description, setDescription] =  useState("")
	const [price, setPrice] =  useState("")
	const [submit, setSubmit] =  useState("false")

 	const [show, setShow] = useState(false);
 
 	const [adminData, setAdminData] = useState([])
	let admin = []
	useEffect(() => {

		fetch('https://tranquil-taiga-72835.herokuapp.com/products')
		.then(res => res.json())
		.then(data => {
			setAdminData(data)
		})

		if(name !== "" && description !== "" && price !== ""){
			setSubmit(true)
		}else{
			setSubmit(false)
		}

	}, [ name, description, price])
 
	admin = adminData.map(product => {
		return(

			<AdminItem key={product._id} adminProp={product}/>
		)
	})
 
 	const handleClose = () => setShow(false);
  	const handleShow = () => setShow(true);



 	const addProduct = (e) => {
		e.preventDefault()
		fetch('http://localhost:4000/products', {
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
					name: name,
					description: description,
					price: price
					
		})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
					console.log(data)
					Swal.fire({
						title: "Add Product successful",
						icon: 'success',
						text: 'You have been successfully add product'
					})
					console.log(data)
					setName("")
					setDescription("")
					setPrice("")
					
				}else{
					Swal.fire({
						title: "Add product failed",
						icon: 'error',
						text: 'Your product has not been added'
					})
				}
			})
	}



 
	return(
		<>
			
			


			<Container>

			<div className="text-center my-4">
				<h2>Admin Dashboard</h2>
				<div className="d-flex justify-content-center">
					<Button variant="primary" onClick={handleShow}>Add New Product</Button>			
				</div>			
			</div>
			<Table striped bordered hover >
				<thead className="bg-dark text-white">
					<tr>
						<td>Name</td>
						<td>Description</td>
						<td>Price</td>
						<td>Availability</td>
						<td>Actions</td>
					</tr>					
				</thead>
				<tbody>
					{ admin }

				</tbody>
			</Table>
			
			{/*Add Modal*/}
			<Modal show={show} onHide={handleClose}>
				<Form  onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Course</Modal.Title>
					</Modal.Header>
					<Modal.Body>	
						<Form.Group controlId="courseName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" required value={name} onChange={e => setName(e.target.value)}/>
						</Form.Group>
						<Form.Group controlId="courseDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" required value={description} onChange={e => setDescription(e.target.value)}/>
						</Form.Group>
						<Form.Group controlId="coursePrice">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" required value={price} onChange={e => parseInt(setPrice(e.target.value))}/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary"  onClick={handleClose}>Close</Button>

					{(submit)
						?
						<Button variant="success" type="submit">Submit</Button>
						:
						<Button variant="success" disabled>Submit</Button>
					}
					</Modal.Footer>
				</Form>
			</Modal>	
		</Container>
		</>
	)

	
} 

