import { useState, useEffect } from 'react';
import HistoryItem from '../components/HistoryItem';

import { Container} from 'react-bootstrap';
import '../checkoutHistoryStyles.css'

export default function CheckoutHistory(){
	const [historyData, sethistoryData] = useState([])
	let histories = []
	// useEffect(() => {

	// 	fetch('https://tranquil-taiga-72835.herokuapp.com/products')
	// 	.then(res => res.json())
	// 	.then(data => {
	// 		console.log(data)

	// 		sethistoryData(data)

	// 	})
	// }, [])


	useEffect(() => {
		fetch('https://tranquil-taiga-72835.herokuapp.com/orders/history', {
		  headers: {
		   Authorization: `Bearer ${ localStorage.getItem('token') }`
		  }
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			sethistoryData(data)
		})

	}, [])
 
	histories = historyData.map(history => {
		return(

			<HistoryItem key={history._id} historyProp={history}/>
		)
	})


	return(
		<>
			<Container>
				<h1>Buying History</h1>
				<ul className="history-listing__list">
					{ histories }
				</ul>
				
			</Container>
		</>
	) 

	
} 


// useEffect(() => {
 		 
	
// 		fetch('https://tranquil-taiga-72835.herokuapp.com/orders/history', {
// 		  headers: {
// 		   Authorization: `Bearer ${ localStorage.getItem('token') }`
// 		  }
// 		})
// 		.then(res => res.json())
// 		.then(data => {
// 			console.log(data)
// 			setHistoryData(data)
// 		})

// 	}, [])