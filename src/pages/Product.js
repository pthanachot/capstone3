import { useState, useEffect } from 'react';
import ProductCard from '../components/ProductCard';
import { Container } from 'react-bootstrap';
import '../productStyles.css'

export default function Product(){
	const [productsData, setProductsData] = useState([])
	let products = []
	useEffect(() => {

		fetch('https://tranquil-taiga-72835.herokuapp.com/products')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProductsData(data)

			var retrievedObject = localStorage.getItem(data._id);

  			console.log('retrievedObject: ', JSON.parse(retrievedObject));
  			setProductsData(data)
		})
	}, [])
 
	products = productsData.map(product => {
		return(

			<ProductCard key={product._id} productProp={product}/>
		)
	})
 
	return(
		<>
			<Container>
				<h1>Product Page</h1>
				<ul className="product-listing__list">
					{products}
				</ul>
			</Container>
		</>
	)

	
} 