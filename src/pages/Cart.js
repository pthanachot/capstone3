import { useState, useEffect } from 'react';
import CartItem from '../components/CartItem';

import { Container, Table, Button, Modal, Form } from 'react-bootstrap';

import '../cartStyles.css'

export default function Cart(){
 
 	 // const [cartData, setCartData] = useState([])
 
 	const [cartsData, setCartsData] = useState([])
 	const [dummy, setDummy] = useState("")

 	const [total, setTotal] = useState('3')

	let carts = []
	useEffect(() => {

		fetch('https://tranquil-taiga-72835.herokuapp.com/products')
		.then(res => res.json())
		.then(data => {
			

			// let i = 0;
			// while (i < data.length) {
  			for (let i = 0; i < data.length; i++) {	
  				var retrievedObject = localStorage.getItem(data[i]._id);

  				
 
  				if (retrievedObject !== null && data[i]._id == JSON.parse(retrievedObject).productId) {
  					
			
  					console.log(data[i])
  				}
				
				console.log(data.filter(function(){
				  return retrievedObject !== null && data[i]._id == JSON.parse(retrievedObject).productId; // keep numbers divisible by 2
				}));

  			}

  			let data_local = [data[2], data[3], data[4], data[5]]

  			// console.log(data_t)


  			setCartsData(data_local)

		})

	


	}, [total])
 
	carts = cartsData.map(product => {
		return(

			<CartItem key={product._id} cartProp={product}/>
		)
	})


	return(
		<>
			<Container>
				<div className="text-center my-4">
				<h2>Cart Item</h2>
							
				</div>

				<Table striped bordered hover >
				<thead className="bg-dark text-white">
					<tr>
						<td>Name</td>
						<td>Price</td>
						<td>Quantity</td>
						<td>Actions</td>
					</tr>					
				</thead>
				<tbody>
					{carts}
					<tr>
						<th>
							<div className="d-flex justify-content-end my-3">
								<Button variant="success">Check Out</Button>	
							</div>
						</th>	
						<th>
							<span>Total: {total}</span>
						</th>
						

					</tr>
				</tbody>

				</Table>
			</Container>
		</>

	)

	
} 