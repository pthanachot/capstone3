// import { Jumbotron, Button, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext'

import { useState, useEffect, useContext } from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import "../productStyles.css";

export default function ProductCard({ productProp }) {

  const { _id, name, description, price } = productProp;

  const [cart, setCart] = useState([])
  const [element, setElement] = useState({})
 

//   this.setState(prevState => ({
//   myArray: [...prevState.myArray, {"name": "object"}]
// }))
 // const addCart = () => {

 //      element.id = _id;
 //      element.quantity = 1;

 //     setCart([...cart, element]);
    

   
 //     console.log(cart)
 //     localStorage.setItem('products', JSON.stringify(cart));
      

 //  }
      
    const addCart = () => {

     localStorage.setItem( _id , JSON.stringify({ productId: _id, quantity: "1" }));
     }


  useEffect(() => {

    
    
  }, [cart, element]) //Anything you add to the array inside of a useEffect is considered the state (or states) that it is managing

    

  return(
      
        <li className="product-listing__item">
          <Card style={{ width: '18rem' }}>
            <Card.Body>
              <Card.Title>{ name }</Card.Title>
              <Card.Text>
                {description}
              </Card.Text>
             
              <Button variant="primary" onClick={addCart}>Add to Cart</Button>
            </Card.Body>
          </Card>
          
        </li>


  )
}  