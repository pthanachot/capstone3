// import { 
// 	Navbar, 
// 	Nav, 
// 	Col, 
// 	Row, 
// 	Form, 
// 	Container 
// } from 'react-bootstrap';
// We can do this, so we can minimize reading code from Left to Right
// because a great way of reading code is Top to Bottom

// And Instead of this code, you can improve the code as below 
// import { Navbar, Nav } from 'react-bootstrap';

import { useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';

import UserContext from '../UserContext';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
 

export default function AppNavbar() {
	const { user ,unsetUser} = useContext(UserContext);

	const logout = () => {
		unsetUser();
		// Add code that will redirect to login page.
		// Do not use window.location.href code
		// Hint: look up the keyword 'history' in React Router docs.
	}


	// const leftNav = (user.token === null) ? (





	const rightNav = (user.token === null) ? (
			<>
				<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
				<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
			</>
		) : (
			<>
				<Nav.Link onClick={logout}>Logout</Nav.Link>
			</>
		);
 

	return (
	    <Navbar bg="light" expand="lg">
		    <Navbar.Brand as={Link} to="/">React Ecommerce</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav"/>
		    <Navbar.Collapse id="basic-navbar-nav">
			    <Nav className="mr-auto">
				    <Nav.Link as={NavLink} to="/">Home</Nav.Link>
				    <Nav.Link as={NavLink} to="/product">Product</Nav.Link>
				    <Nav.Link as={NavLink} to="/cart">Cart</Nav.Link>
				    <Nav.Link as={NavLink} to="/admin">Admin</Nav.Link>
				    <Nav.Link as={NavLink} to="/checkout">History</Nav.Link>
			    </Nav>
			    <Nav className="ml-auto">
			    	{rightNav}
			    </Nav>
		    </Navbar.Collapse>
	    </Navbar>		
	);
}
