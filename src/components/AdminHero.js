import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';



export default function AdminHero(){
	return(
		<Row>
			<Col xs={12} md={8} className="mx-auto">
				<div className="">
					<Card.Body className="text-center">
						<Card.Title>
							<h1>Admin Dashboard</h1>
						</Card.Title>
						<Card.Text>
							  <Button variant="primary">Add New Product</Button>{' '}
							  <Button variant="success">Show User Orders</Button>{' '}
						</Card.Text>
					</Card.Body>					
				</div>				
			</Col>
		</Row>
				
	)
}