import { useState, useEffect } from 'react';
import { Container, Table, Button, Modal, Form } from 'react-bootstrap';

import "../adminDashboardStyles.css"

export default function AdminItem({ adminProp }) {
 

	// Set State

	const [quantity, setQuantity] = useState(1);
	const [show, setShow] = useState(false);
 
 	const { name, price, description } = adminProp;


	// UseEffect
	useEffect(() => {
		
	}, [])
 

	// Function

	const plus = () => {
		setQuantity(quantity + 1);
	}

	const minus = () => {
		if (quantity === 1) {
			setShow(true);
		} else {
			setQuantity(quantity - 1);
		}	
	}

 	const handleClose = () => setShow(false);
  	const handleShow = () => setShow(true);


  return(
       
	

					<tr>
						<th>
							<div className="admin-listing__item-main">
								<figure className="admin-listing__item-figure"></figure>
								<div className="admin-listing__item-title"><span>{ name }  </span></div>
							</div>
						</th>
						<th>
							<div className="admin-listing__item-description">
								<div className="admin-listing__item-text"><span>{ description }</span></div>
							</div>
						</th>
						<th>
							<div className="admin-listing__item-price">	
								<span> { price } </span>
							</div>
						</th>
						<th>Availability</th>
						<th>
							<Button className="admin-listing__item-btn-update" >Update</Button>
							<Button className="admin-listing__item-btn-disable">Disable</Button>
		
						</th>
					</tr>
  )
}  

{/*<li className="admin-listing__item">
		<div className="admin-listing__item-main">
			<figure className="admin-listing__item-figure"></figure>
			<div className="admin-listing__item-title"><span>{ name }  </span></div>
		</div>

		<div className="admin-listing__item-description">
			<div className="admin-listing__item-text"><span>{ description }</span></div>
		</div>
		

		<div className="admin-listing__item-price">	
			<span> { price } </span>
		</div>
		<div className="admin-listing__item-quantity">
			<span> Available </span>
		</div>
		<div className="admin-listing__item-action">
			<Button className="admin-listing__item-btn-update" >Update</Button>
			<Button className="admin-listing__item-btn-disable">Disable</Button>
			
		</div>
	</li>*/}