// import { Jumbotron, Button, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext'

import { useState, useEffect, useContext } from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import "../productStyles.css";

export default function HistoryItem({ historyProp }) {

  const { _id, totalAmount, purchaseOn, products } = historyProp;
  const [productlist, setProductlist] = useState('')
 
  useEffect(() => {


   console.log(products)
  }, []) //Anything you add to the array inside of a useEffect is considered the state (or states) that it is managing

    

  return(
      
       <Row>
			<li className="history-listing__item">
						<div className="history-listing__order">
							<div>Purchase On: {purchaseOn}</div>
						</div>
						<div className="history-listing__description">
							<div>Items:</div>
							<ul>

								<li>Product Id: {products[0].productId}</li>

							</ul>
							<div>Total: {totalAmount}</div>
						</div>
					</li>
					
		</Row>


  )
}  
