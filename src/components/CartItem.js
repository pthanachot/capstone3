import { useState, useEffect } from 'react';
import { Button, Modal } from 'react-bootstrap';

import "../cartStyles.css"

export default function CartItem({ cartProp }) {
 

	// Set State

	const [quantity, setQuantity] = useState(1);
	const [show, setShow] = useState(false);

	const [total, setTotal] = useState("");
 
 	const { name, price } = cartProp;


	// UseEffect
	useEffect(() => {
		
	}, [])


	// Function

	const plus = () => {
		setQuantity(quantity + 1);
	}

	const minus = () => {
		if (quantity === 1) {
			setShow(true);
		} else {
			setQuantity(quantity - 1);
		}	
	}

 	const handleClose = () => setShow(false);
  	const handleShow = () => setShow(true);


  	const totalPrice = () => {
		setTotal(parseInt(quantity) * parseInt(price));	
		console.log(total)
	}


  return(
  	<tr>
		<th>
			<div className="cart-listing__item-main">
				<figure className="cart-listing__item-figure"></figure>
				<div className="cart-listing__item-title"><span>{ name }  </span></div>
			</div>
		</th>
		<th>
			<div className="cart-listing__item-price">	
				<span> { price } </span>
			</div>
		</th>
		<th>
			<div className="cart-listing__item-quantity">
				<Button className="cart-listing__item-price-minus" onClick={minus}>-</Button>
				<input className="cart-listing__item-price-input" type="number" onChange={(e) => setQuantity(e.target.value)} value={quantity} />
				<Button className="cart-listing__item-price-plus" onClick={plus}>+</Button>
			</div>
		</th>
	 
		<th>
			<div className="cart-listing__item-action">
			<Button className="cart-listing__item-btn-delete" onClick={handleShow}>Delete</Button>
			<Modal show={show} onHide={handleClose}>
		        <Modal.Header closeButton>
		          <Modal.Title>Warning</Modal.Title>
		        </Modal.Header>
		        <Modal.Body>Do you really want to delete item from the cart?</Modal.Body>
		        <Modal.Footer>
		          <Button variant="secondary" onClick={handleClose}>
		            Cancel
		          </Button>
		          <Button variant="primary" onClick={handleClose}>
		            Delete
		          </Button>
		        </Modal.Footer>
		    </Modal>
		</div>

		</th>
	</tr>
    
  )
}  