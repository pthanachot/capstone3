// import { Jumbotron, Button, Row, Col } from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import { Link, NavLink } from 'react-router-dom';

export default function Banner() {
	return(
		<Row>
			<Col>
				<Jumbotron>
					<h1>Electric Shop</h1>
					<p>Find the best deal for your Computer!</p>
					<Nav.Link as={NavLink} to="/product"><Button>Shop Now!</Button></Nav.Link>
				</Jumbotron>
			</Col>
		</Row>
	)
}