
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

export default function Highlights(){
	return(
		<Row>
			<Col xs={12} md={4}>
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title>
							<h2>The Best Electric shop in Asia</h2>
						</Card.Title>
						<Card.Text>
							With over decades of experience, we offer the best service to satisfy our customer. 
							What you can find from here? Anything about electric things you want. Moreover, we have
							24 hours call center service and money back garuntee.
						</Card.Text>
					</Card.Body>					
				</Card>				
			</Col>
			<Col xs={12} md={4}>
				<Card className="course-highlight">
					<Card.Body>
						<Card.Title>
							<h2>Get The Best Deal</h2>
						</Card.Title>
						<Card.Text>
							In our electricshop, you will get the best deal avaialble on the internet.
							Out company is specialized in this field for decades, so we have partners and communities that 
							offer you great price with high quality product. Moreover, most of our product is directly from 
							the manufacturing.  
						</Card.Text>
					</Card.Body>					
				</Card>				
			</Col>
			<Col xs={12} md={4}>
				<Card className="course-highlight">
					<Card.Body>
						<Card.Title>
							<h2>Pay in Seconds</h2>
						</Card.Title>
						<Card.Text>
							Do you worry about payment? Here, we have all the options you need 
							including Paypal, Debit & Credit Card, Bank Transfer, and even Crypto.
							Of course, you need to check whether those works on your country and the 
							fee is varaied based on the country. All we garuntee that you can pay in seconds. 
						</Card.Text>
					</Card.Body>					
				</Card>				
			</Col>
		</Row>
	)
}