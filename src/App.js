import { useState, useEffect, useContext } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import UserContext from './UserContext'

import AppNavbar from './components/AppNavbar';

import Home from './pages/Home';
import Login from './pages/Login';
import NotFound from './pages/NotFound';
import Register from './pages/Register';
import Product from './pages/Product';
import Cart from './pages/Cart';
import Admin from './pages/AdminDashboard';
import Checkout from './pages/CheckoutHistory';

import { UserProvider } from './UserContext';

//use destructuring to isolate only the react-bootstrap components that we need

//App entry point
//JSX

  

export default function App() {
  // Creating a user state from App component provides a 'single source of truth'
  const [user, setUser] = useState ({
    token: localStorage.getItem('token')
    // So every state use data from this. Very Effective!
  });

  // Function to be used for logging out
  const unsetUser = () => {
    localStorage.clear();
    setUser({ token: null});
  }
 
  return(
    
    <UserContext.Provider value={{ user, setUser, unsetUser }}>
      <BrowserRouter>
        <AppNavbar/>
        <Routes>
          <Route exact path="/" element={<Home/>}/>
          <Route exact path="/login" element={<Login/>}/>
          <Route exact path="/register" element={<Register/>}/>
          <Route exact path="/product" element={<Product/>}/>
          <Route exact path="/cart" element={<Cart/>}/>
          <Route exact path="/admin" element={<Admin/>}/>
          <Route exact path="/checkout" element={<Checkout/>}/>
          

          <Route path="*" element={<NotFound/>}/>
          
        </Routes>
      </BrowserRouter>
    </UserContext.Provider>
  );
}
